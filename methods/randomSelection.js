// module for the random selection of points

// method assigns a random amount of items to each driver
module.exports = function(items, drivers, variation){

    let itemsArray = JSON.parse(JSON.stringify(items));

    let average = Math.round(items.length/drivers.length);
    // sets the max and min items a driver can have
    let max = Math.round(average + (average*variation/100));
    let min  = Math.round(average - (average*variation/100));

    let numberOfItems;
    let res = [];
    let tempRes = {};

    for (let i = 0; i < drivers.length; i++) {
        let tempRes = {};
        tempRes["driver"] = drivers[i];
        tempRes["items"] = [];

        // handles error case when max is not adjusted, prevents from assigning more items than there are
        if(itemsArray.length < max && itemsArray.length > average) max = itemsArray.length;

        // if last driver or not much left, assign everything
        if(itemsArray.length <= average || i == drivers.length-1) numberOfItems = itemsArray.length;        
        else numberOfItems = getRandomIntInclusive(min, max);

        // takes random items in the list and assigns it to the current driver
        for (let j = 0; j < numberOfItems; j++) {
            tempRes["items"].push(itemsArray.splice(getRandomInt(itemsArray.length-1), 1)[0]);
        }

        res.push(tempRes);
    }

    return res;
}

// returns an int between min (included) and max (included)
function getRandomIntInclusive(min, max) {
    let res = Math.floor(Math.random() * (max - min +1)) + min;
    return res;
}

// returns int between 0 (included) and max (included)
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}