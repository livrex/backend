const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 8000;

const distrib = require('./methods/pathDistribution');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
    const allowedOrigins = ['https://infomaniak-livrex1.web.app', 'http://localhost:4200', 'https://livrex.web.app', 'https://livrex.firebaseapp.com'];
    const origin = req.headers.origin;
    if (allowedOrigins.includes(origin)) {
        res.header('Access-Control-Allow-Origin', origin);
    }
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'GET,POST');
    next();
});

app.get('/hello', function(req, res){
    res.send('aloha');
})

app.post('/spt', function(req, res) {
    res.send(distrib(req.body.items, req.body.drivers));
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
});