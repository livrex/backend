// module to compute the standard deviation (ecart-type) in a list of elements

module.exports = function(values){
  let avg = average(values);
  
  let squareDiffs = values.map(function(value){
    let diff = value - avg;
    let sqrDiff = diff * diff;
    return sqrDiff;
  });
  
  let avgSquareDiff = average(squareDiffs);

  let stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

function average(data){
  let sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  let avg = sum / data.length;
  return avg;
}