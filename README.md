# Livrex

Livrex est un prototype d'un projet web permettant de répartir la livraison de paquets entre plusieurs livreurs. Ceci est la partie backend implémentée à l'aide de Node.js qui est déployé à l'adresse suivante : https://livrex-backend.herokuapp.com/

## Concept
Ce backend s'occupe donc de répartir les items entre les différents conducteurs et de définir le trajet à prendre, cela en esseyant de faire en sorte qu'il n'y ait pas trop de différences en km entre les différents trajets. Certains items peuvent être également retirés et repoussés pour la prochaine livraison s'il n'est pas possible de limiter les trajets à 240km.

## Démarche
Ma première idée instinctivement a été d'utiliser l'aléa pour répartir les items et de tester de manière itérative. En effet, j'ai pensé que cela pourrait initialement me simplifier le problème afin d'avoir une première version qui fonctionne <br><br>
Donc, je me suis donc principalement concentré sur la recherche du plus court chemin entre un nombre de points donnés. La première méthode qui m'est venue à l'esprit a été de chercher les permutations possibles entre les points, comme les points pouvaient être dans n'importe quel ordre. J'ai donc implémenté la version récursive de l'algorithme de Heap, avec un seuil pour limiter l'exploration.<br><br>
Cependant, je me suis assez vite rendu compte que cette solution n'était pas du tout optimale et commençait à prendre beaucoup de temps lorsqu'un trajet avait plus de 7 items à distribuer. C'est pourquoi j'ai cherché d'autres solutions et j'ai découvert la méthode séparation et évaluation (Branch and Bound). C'est une méthode qui utilise la réduction de la matrice des distances d'un chemin afin de limiter fortement l'exploration. Elle retourne donc pas forcément la meilleure solution, mais la solution retournée reste satisfaisant et la complexité en temps s'en retrouve grandement réduite.<br><br>
Ma solution finale combine donc l'assignation d'tems aléatoire et le calcul du plus court chemin, le tout dans une boucle itérative afin de trouver une réponse optimale.