//Implementation of shortest path search using Branch and Bound

const d = require('./distance');

class BBState{
    constructor(parent, info, toVisit, matrix, cost, index){
        this.parent = parent;
        this.info = info; // the JSON with all the info
        this.toVisit = toVisit;
        this.matrix = matrix;
        this.cost = cost;
        this.index = index;
    }
}

module.exports = function (items, driver){

    let upperBound = Infinity;
    const startPosition = {type: 'start', id: 'start', x: 0, y: 0}; // start position

    // computes the distance between all the points to create the initial matrix
    function createDistanceMatrix(startPosition, items){
        let completeItems = [startPosition].concat([...items]); // completing it with the start position
        let distMatrix = [];
        for (let j = 0; j < completeItems.length; j++) {
            let column = [];
            for (let i = 0; i < completeItems.length; i++) {
                if(i === j) column.push(Infinity);
                else column.push(d.distance2points(completeItems[j], completeItems[i]));
            }
            distMatrix.push(column);
        }
        return distMatrix;
    }

    // sets the row and column concerned to infinity to indicate that those points won't be reused
    // prepares for reduction
    function preReducingMatrix(distMatrix, origin, dest){
        for (let j = 0; j < distMatrix.length; j++) {
            distMatrix[j][origin] = Infinity;
        }
        distMatrix[dest].forEach((el, index, arr) => arr[index] = Infinity);
        return distMatrix;
    }

    // reduces the matrix by row and column using the smallest value of the row or column
    function reduceMatrix(distMatrix){
        //row reduction
        let rowSum, colSum;
        rowSum = colSum = 0;
        for (let i = 0; i < distMatrix.length; i++) {
            let rowMin = Infinity;
            for (let j = 0; j < distMatrix.length; j++) {
                if(rowMin > distMatrix[j][i]) rowMin = distMatrix[j][i]
            }

            if(rowMin === Infinity) continue;
            else rowSum += rowMin;

            for (let j = 0; j < distMatrix.length; j++) {
                distMatrix[j][i] -= rowMin;
            }
        }

        //column reduction
        for (let j = 0; j < distMatrix.length; j++) {
            let colMin = Math.min(...distMatrix[j]);
            
            if(colMin === Infinity) continue;
            else colSum += colMin;

            distMatrix[j].forEach((el, index, arr) => arr[index] -= colMin);
        }
        return {reduction: (colSum + rowSum), distMatrix};
    }
    
    // implementation of a standard search algorithm with a queue that is sorted with Branch and Bound
    function search(startPosition, items){
        let initMatrix = reduceMatrix(createDistanceMatrix(startPosition, items));
        let queue = [];
        for (let i = 0; i < items.length; i++) {
            items[i]['order'] = i+1; // adding the index to the JSON for easier use with the matrix
        }
        let initialState = new BBState(null, startPosition, items, initMatrix.distMatrix, initMatrix.reduction, 0); //new State(null, startPosition, items, 0);
        queue.push(initialState);
        let currentState = new BBState(null, null, null, null, null, null);

        // the final result
        let res = {
            path: [],
            distance: {edges: [], sum: Infinity}
        };
        // temporary result
        let tempPath = []; let tempDistance = 0;

    
        while (queue.length != 0) {
          currentState = queue.shift();
          
          if (currentState.info.type == 'driver' && currentState.index === -1) { // goal is hit
            backtrack(currentState).reverse().forEach(x => tempPath.push(x)); // reverse because the list is in the inverse order
            tempDistance = d.fullDistance(tempPath);
            if(currentState.parent.cost < upperBound) {
                upperBound = currentState.parent.cost;
                res.distance = tempDistance;
                res.path = tempPath;
                queue = queue.filter((el) => el.cost < upperBound); // filtering based on new upperBound
            }
            tempPath = [];
          } else {
            queue = queue.concat(conditions(currentState));//conditions(currentState).forEach(x => {if(x.length !== 0) queue.push(x)}) //adding children 
            queue.sort((a, b) => a.cost - b.cost); // sorting by least cost
          }
        }
    
        return res;
    
    }

    // method to create new children states based on certain conditions
    function conditions(e){
        let enfants = [];
        let visitsClone = [];
    
        if(e.toVisit.length === 0 && e.info.type != 'driver'){ // nothing more to explore, goal is hit
            enfants.push(new BBState(e, driver, null, null, -1, -1)); // we can now add the final state
            return enfants;
        }
    
        for (let i=0; i < e.toVisit.length; i++) {
            visitsClone = [...e.toVisit]; // cloning
            visitsClone.splice(i, 1); // remove the child we are processing, so that it doesn't have itself in its state
            let nMatrix = reduceMatrix(preReducingMatrix(JSON.parse(JSON.stringify(e.matrix)), e.index, i+1)); // have to JSON parse / stringify for arrays that are more than 1 dimension
            enfants.push(new BBState(
                e,
                e.toVisit[i],
                visitsClone,
                nMatrix.distMatrix,
                e.matrix[e.toVisit[i].order][e.index] + e.cost + nMatrix.reduction, // cost formula
                e.toVisit[i].order
            ))
        };

        return enfants;
    
    }

    return search(startPosition, items, driver); // launching the search
}

// method to get the path
function backtrack(e) {
    let res = [];

    res.push(e.info);

    if (e.parent != null) {
      res = res.concat(backtrack(e.parent));
    }
    return res
}