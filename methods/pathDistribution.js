const backtrack = require('./BBspt')
const rs = require('./randomSelection');
const sd = require('./standardDeviation');

// method searching for the optimal items distribution between the drivers
module.exports = function(items, drivers){ 
    
    let processedData = []; // final result
    let tempData = []; // temp result
    let tempDeviation = 0;
    let deviation = Infinity;
    let sumArray = [];
    let i = 0;
    let percentageDiff = 100;
    let maxSum = 0;
    let minSum = Infinity;
    let mainIterationCount = 1000;
    let tempForTomorrow = [];
    let itemsCopy = [];
    let adequateRemoval = false;
    let adequateLength = 0;
    let iteratorLength = 0;
    let currentNbItems = 0;
    let forTomorrow = [];
    
    itemsCopy = JSON.parse(JSON.stringify(items)); // copy
    
    if(items.length/drivers.length > 7){
        mainIterationCount = 500; // reduce the number of iteration, in order for it to be faster
    }
    
    if(drivers.length === 1){ // special more optimal iteration cycle for only one driver
        while (i < 1000) { // one cycle represents one distribution

            //helps the algorithm try different solutions
            let processedParams = rs(itemsCopy, drivers, 50)
            
            tempSum = 0;
            tempData = [];

            //only one param
            for (param of processedParams) {
                temp = backtrack(param.items, param.driver);
                tempData.push(temp);
                tempSum += temp.distance.sum; // get the sum
            }    
            
            // only accepting optimal solutions compared to the previous one to a certain degree
            if((currentNbItems < temp.path.length && temp.distance.sum < 240) || (currentNbItems==temp.path.length && minSum > temp.distance.sum)){
                minSum = temp.distance.sum;
                processedData = tempData;
                currentNbItems = temp.path.length;
                forTomorrow = [...tempForTomorrow];
            } 
            
            // if the sum is to high (240km limit), remove more and more items at random.
            // once we get a valid sum, keep in memory the number of items to remove and only remove that much every time
            if(adequateRemoval || tempSum > (240*drivers.length)){
                itemsCopy = JSON.parse(JSON.stringify(items));
                if(adequateRemoval) iteratorLength = adequateLength - Math.floor(Math.random() * 4); //wider range of tests
                else iteratorLength = tempForTomorrow.length+1;
                tempForTomorrow = [];
                for (let j = 0; j < iteratorLength; j++) {
                    tempForTomorrow = tempForTomorrow.concat(itemsCopy.splice(Math.floor(Math.random() * itemsCopy.length-1), 1));
                }
            }else{ // valid sum
                adequateRemoval = true;
                adequateLength = iteratorLength;
            }            
            
            i++;
        } 
        
    } else{ // multiple drivers
        let minSum = Infinity;
        let tempSum;
        while (i < mainIterationCount) {
            
            let processedParams = rs(itemsCopy, drivers, 50)
            
            maxSum = 0;
            tempSum = 0;
            tempData = [];
            sumArray = [];
            for (param of processedParams) { // for each driver, calculate the shortest path through their assigned points
                temp = backtrack(param.items, param.driver);
                tempData.push(temp);
                sumArray.push(temp.distance.sum); // sum array to compute deviation between the paths
                tempSum += temp.distance.sum;
                if(maxSum < temp.distance.sum) maxSum = temp.distance.sum;
            }
            tempDeviation = sd(sumArray);
            
            // only accepting optimal solutions compared to the previous one to a certain degree
            // the parameters could be adjusted for a more optimal use
            if((maxSum < 240) && ( (currentNbItems < temp.path.length && tempDeviation/maxSum - percentageDiff < 0.03) || (tempDeviation < deviation) ) ) {
                deviation = tempDeviation;
                processedData = tempData;
                minSum = tempSum;
                percentageDiff = deviation/maxSum;
                currentNbItems = temp.path.length;
                forTomorrow = [...tempForTomorrow]; // copy
            }
            
            //same as for 1 driver
            if(adequateRemoval || tempSum > (240*drivers.length)){
                itemsCopy = JSON.parse(JSON.stringify(items));
                if(adequateRemoval) iteratorLength = adequateLength - Math.floor(Math.random() * 4); //wider range of tests
                else iteratorLength = tempForTomorrow.length+1;
                tempForTomorrow = [];
                for (let j = 0; j < iteratorLength; j++) {
                    tempForTomorrow = tempForTomorrow.concat(itemsCopy.splice(Math.floor(Math.random() * itemsCopy.length-1), 1));
                }
            }else{
                adequateRemoval = true;
                adequateLength = iteratorLength;
            }
                        
            i++;
        } 
    }

    return {today: processedData, tomorrow: forTomorrow};
}