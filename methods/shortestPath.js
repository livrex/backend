// First algorithm used, now unused, was replaced with BBspt as it is much faster
// Implementation of Heap's permutation to test possible routes to find optimal path

const d = require('./distance');

module.exports = function(items, driver){

    const startPosition = {type: 'start', id: 'start', x: 0, y: 0}; // start position
    let result = {
        path: null,
        distance: {edges: [], sum: Infinity}
    };
    let upperBound = Infinity;
    
    //recursive method computing all the permutations of a sequence and calculating the distance of each permutation
    function heapPerm(n, items) {
        // upperbound test limiting useless explorations
        if(d.fullDistance(concatenate(startPosition, items.slice(0, items.length-n), driver)).sum > upperBound) return;
        
        if(n === 1 || n === 0){ // all elements placed for this sequence, got a permutation

            //adding start and end positions
            var temp = concatenate(startPosition, items, driver);

            // assigning new path if more optimal
            let newDistance = d.fullDistance(temp);
            if(newDistance.sum < upperBound){
                result.distance = newDistance;
                result.path = temp;
                upperBound = newDistance.sum;
            }

            return;
        }
        heapPerm(n-1, items); // recursion until one element

        for (let i = 0; i < n-1; i++){
            // the parity defines the correct swap to make
            if(n % 2 === 0) swap(items, i, n-1);
            else swap(items, 0, n-1);    
            heapPerm(n-1, items);
        }
    }

    heapPerm(items.length, items); // starting heap
    return result;
}

// simple method to swap to elements in an array
function swap(items, i, j){
    temp = items[i];
    items[i] = items[j];
    items[j] = temp;
}

// method to clone and concatenate the elements
function concatenate(startPosition, items, driver){
    let temp = [startPosition];
    temp = temp.concat(JSON.parse(JSON.stringify(items)));
    temp.push(driver);
    return temp;
}