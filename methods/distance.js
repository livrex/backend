// methods to get distance between two points or a set of points

module.exports = {
    fullDistance,
    distance2points
}

function fullDistance(positionsArr){
    let edges = []; // also returning the individual distances between the points that make up the path
    let sum = 0;
    if(positionsArr.length === 1){ // if only 1 point
        return 0;
    } else {
        for (let i = 0; i < positionsArr.length-1; i++) {
            var temp = distance2points(positionsArr[i], positionsArr[i+1]);
            edges.push(temp);
            sum += temp;
        }
    }
    return {edges, sum};
}

function distance2points(p1, p2){
    return (Math.sqrt((p2.x - p1.x)**2 + (p2.y - p1.y)**2));
}